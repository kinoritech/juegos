"use strict";
const axios = require('axios');
const { Server, FlatFile } = require('boardgame.io/server');

const config = require('./environments/config')
const kpino = require('koa-pino-logger')
const pino = require('pino');

const dest = pino.destination(config.logDestination);
const klog = kpino({name: 'oatuh', level: config.loglevel}, dest);
const log = pino({name: 'usuarios', level: config.loglevel}, dest);

const solicitarCredenciales = async ctx => {
    const fichaPortador = ctx.request.headers['authorization'];
    ctx.log.info('solicitar Credenciales %s:%s', ctx.request.body.playerName, fichaPortador);
    const options = {
        url: `${config.authServer}/auth/${ctx.request.body.playerName}/credenciales`,
        method: 'get',
        headers: {
            'authorization': fichaPortador,
        }
    };
    ctx.log.info('solicitar Credenciales url %s', config.authServer);
    return await axios(options)
        .then(res => res.data)
        .catch(err => {
            ctx.log.error('No se encontró el servidor de OAuth. %s', err.message);
            throw(err);
        });
}

const autenticarCredenciales = async (credentials, playerMetadata) => {
    log.info('autenticar Credenciales %s:%s', playerMetadata.name, credentials);
    if (credentials) {
        const options = {
            url: `${config.authServer}/auth/${playerMetadata.name}/validar-credenciales`,
            method: 'post',
            data: { credentials },
        };
        return axios(options)
            .then(res => {
                const validas = res.status === 202;
                log.info('Resultado autenticacion %s', validas)
                return validas;
            })
            .catch(err => {
                log.error('No se encontró el servidor de OAuth. %s', err);
                return false;
            });

    }
    log.warn('autenticar Credenciales no espera axios')
    return false;
}

const parques = require('./juegos/parques.data');
const server = Server({
    games: [parques.Parques8P, parques.Parques6P, parques.Parques4P],
    generateCredentials: solicitarCredenciales,
    authenticateCredentials: autenticarCredenciales,
    db: new FlatFile({
        dir: config.storedGamesFolder,
        logging: true,
        ttl: false,
      }),
  });

// Agregar pino al app
server.app.use(klog);
server.run({port: config.port})
    .then(() => log.info('Empezar a jugar!'))
    .catch(err => log.error('No se pudo iniciar el servidor: %s.', err.message));
