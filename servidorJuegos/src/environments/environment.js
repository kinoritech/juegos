module.exports = {
  port: 8100,
  authServer: 'http://localhost:8102',
  storedGamesFolder: './savedgames',
  loglevel: 'debug',
  logDestination: 'D:\\temp\\logs\\servidorJuegos.log'
}
