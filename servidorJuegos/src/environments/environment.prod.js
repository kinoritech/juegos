module.exports = {
  port: 8000,
  authServer: 'http://localhost:8002',
  storedGamesFolder: 'var/lib/kinoritech/juegos/savedgames',
  loglevel: 'info',
  logDestination: '/var/log/kinoritech/servidorJuegos.log'
}