import { ActivePlayers, TurnOrder } from 'boardgame.io/core';
import _ from "lodash";
/**
 * Clase que representa una ficha en el juego. Cada ficha contiene su posición actual,
 * us id (html) y el jugador al que pertenece.
 */
export class Ficha {
    constructor(cx, cy, id, idJugador) {
        this.cx = cx;
        this.cy = cy;
        this.id = id;
        this.idJugador = idJugador;
    }
}
/**
 * Clase que representa la posición de una ficha en el tiempo. Un arreglo de Movidas representa la trayectoria de
 * una ficha
 */
export class Movida {
    constructor(cx, cy, tipo, marcaTiempo) {
        this.cx = cx;
        this.cy = cy;
        this.tipo = tipo;
        this.marcaTiempo = marcaTiempo;
    }
}
const contextoInicial = {
    fichas: {},
    valorDados: [],
    lanzarDados: 0,
    jugadorMovida: -1,
    fichaMovida: '',
    ultimaMovida: [],
};
/**
 * Estructura base de boardgame.io sobre el juego
 */
const parques = {
    minPlayers: 2,
    moves: {
        ponerFichas,
        lanzarDados,
        moverFicha // Mover una ficha
    },
    phases: {
        inicio: {
            moves: { ping, ponerFichas },
            start: true,
            next: 'jugando',
            endIf: G => (Object.keys(G.fichas).length === G.maxFichas),
            turn: {
                activePlayers: ActivePlayers.ALL,
            }
        },
        jugando: {
            turn: {
                stages: {
                    ping: {
                        moves: { ping }
                    },
                    advance: {
                        moves: { lanzarDados, moverFicha, ping }
                    },
                }
            }
        },
    },
    turn: {
        order: TurnOrder.RESET,
    },
};
/**
 * Parques 8 personas
 */
export const Parques8P = _.assign({
    name: 'Parques-8P',
    maxPlayers: 8,
    setup: ctx => (_.assign({
        alturaFuenteDados: '90px',
        maxFichas: ctx.numPlayers * 4,
    }, contextoInicial)),
}, parques);
/**
 * Parques 6 personas
 */
export const Parques6P = _.assign({
    name: 'Parques-6P',
    maxPlayers: 6,
    setup: ctx => (_.assign({
        alturaFuenteDados: '90px',
        maxFichas: ctx.numPlayers * 4,
    }, contextoInicial)),
}, parques);
/**
 * Parques 4 personas
 */
export const Parques4P = _.assign({
    name: 'Parques-4P',
    maxPlayers: 4,
    setup: ctx => (_.assign({
        alturaFuenteDados: '75px',
        maxFichas: ctx.numPlayers * 4,
    }, contextoInicial)),
}, parques);
/**
 * Genera la secuencia de dados a animar.
 * @param G
 * @param ctx
 */
function lanzarDados(G, ctx) {
    // tslint:disable:no-bitwise
    G.tirarDados = G.tirarDados ^ 1;
    G.valorDados = ctx.random.Die(6, 24);
    G.ultimaMovida = [];
    G.jugadorMovida = -1;
}
function moverFicha(G, ctx, playerId, fichaId, movidas) {
    G.jugadorMovida = playerId;
    G.fichaMovida = fichaId;
    G.ultimaMovida = movidas;
    const match = G.fichas[fichaId];
    if (match !== null) {
        const lastPos = movidas[movidas.length - 1];
        match.cx = lastPos.cx;
        match.cy = lastPos.cy;
    }
}
function ponerFichas(G, ctx, fichas) {
    fichas.forEach(f => {
        G.fichas[f.id] = f;
    });
}
/**
 * Juagada que sirve para sincronizar
 * @param G
 * @param ctx
 * @param fichas
 */
function ping(G, ctx) {
}
//# sourceMappingURL=parques.data.js.map