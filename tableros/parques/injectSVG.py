import sys
import re

xml_attrib = re.compile('([^\s=]+)\s*=\s*(\'[^<\']*\'|"[^<"]*\")')
svg_tag = re.compile('(\s*)<svg\s+([^>]+(?:\"|\'))\s?/?>')
dado_id = re.compile('(\s*)(<text id=\"dado(\d)\")([^\n]+)')
estrella_id = re.compile('(\s*)(<path id="estrella")([^\n]+)')
tirar_id = re.compile('(\s*)(<path id="tirarDados")([^\n]+)')
circle_tag = re.compile('(\s*)(<circle)([^\n]+)')


def imprimir_texto(alineacion, texto, t, nl='\n'):
    """
    Imprime una linea con el texto el el archivo t, utiliznado la alineación dada.
    :param alineacion:
    :param texto:
    :param t:
    :return:
    """
    t.write(f'{alineacion}{texto}{nl}')


def imprimir_attr(key, value, t):
    t.write(f' {key}={value}')


def imprimir_directiva(directiva, t):
    t.write(f' {directiva}')


def cols(param):
    """
    Crea bloques de 4 espacios para alinear el text.
    :param param: NUmero de bloques a crear
    :return:
    """
    return "   " * param


def inyectar_eventos_raton(t, line):
    """
    Le inyectamos al elemento SVG los eventos de ratón y pantalla
    :param t:
    :param line:
    :return:
    """
    match = svg_tag.match(line)
    if match is not None:
        print(line[:-2])
        t.write('<svg')
        align = match.group(1)
        attr = match.group(2)
        if attr:
            for pair in xml_attrib.finditer(attr):
                print(pair.group(1), pair.group(2))
                key = pair.group(1)
                value = pair.group(2)
                if key == 'width':
                    value = '"100%"'
                if key == 'height':
                    key = "viewBox"
                    value = '"0 0 766.6438 766.6438"'
                imprimir_attr(key, value, t)
        t.write('\n')
        imprimir_texto(align + cols(1), '#tablero', t)
        imprimir_texto(align + cols(1), '(mousedown) = "inicioMouse($event)"', t)
        imprimir_texto(align + cols(1), '(touchstart) = "inicioTouch($event)"', t)
        imprimir_texto(align + cols(1), '(mouseup) = "finMouse($event)"', t)
        imprimir_texto(align + cols(1), '(mouseleave) = "finMouse($event)"', t)
        imprimir_texto(align + cols(1), '(touchend) = "finTouch($event)"', t)
        imprimir_texto(align + cols(1), '(touchleave) = "finTouch($event)"', t)
        imprimir_texto(align + cols(1), '(touchcancel) = "finTouch($event)"', t)
        imprimir_texto(align + cols(1), '>', t)
        inyectar_defs(align, t)
        return False
    else:
        return True


def inyectar_defs(align, t):
    """
    Inyecta la fuente de los dados en un 'defs' tag
    :param t:
    :return:
    """
    imprimir_texto(align + cols(1), '<defs>', t)
    imprimir_texto(align + cols(2), '<style>', t)
    imprimir_texto(align + cols(3), '@font-face {', t)
    imprimir_texto(align + cols(4), 'font-family: Berlin Sanns FB;', t)
    imprimir_texto(align + cols(4), 'src: url("/assets/BRLNSB.woff") format(\'woff\');', t)
    imprimir_texto(align + cols(3), '}', t)
    imprimir_texto(align + cols(2), '</style>', t)
    imprimir_texto(align + cols(1), '</defs>', t)


def inyectar_dados(s, t, line):
    """
    Agrega hash a los dados para que angular los encuentre.
    :param s:
    :param t:
    :param line:
    :return:
    """
    match = dado_id.match(line)
    if match is not None:
        align = match.group(1)
        imprimir_texto(align, match.group(2), t, "")
        dice = match.group(3)
        imprimir_directiva(f'#dado{dice}', t)
        imprimir_texto("", match.group(4), t)
        return False
    else:
        return True


def inyectar_estrella(t, line):
    """
    Agrega el evento de click a la estrella
    :param t:
    :param line:
    :return:
    """
    match = estrella_id.match(line)
    if match is not None:
        align = match.group(1)
        imprimir_texto(align, match.group(2), t, "")
        imprimir_directiva('#estrella', t)
        imprimir_attr('(click)', '"enFinTurno($event)"', t)
        imprimir_attr('[ngClass]', '"\'d\' + playerID"', t)
        imprimir_texto("", match.group(3), t)
        return False
    else:
        return True


def inyectar_boton_tirar_dados(t, line):
    """
    Agrega el evento de click al boton de tirar dados
    :param t:
    :param line:
    :return:
    """
    match = tirar_id.match(line)
    if match is not None:
        align = match.group(1)
        imprimir_texto(align, match.group(2), t, "")
        imprimir_directiva('#tirarDados', t)
        imprimir_attr('(click)', '"enTirarDados($event)"', t)
        imprimir_texto("", match.group(3), t)
        return False
    else:
        return True


def injectar_fichas(t, line):
    match = circle_tag.match(line)
    if match is not None:
        align = match.group(1)
        imprimir_texto(align, match.group(2), t, "")
        imprimir_directiva('#ficha', t)
        imprimir_texto("", match.group(3), t)
        return False
    else:
        return True


def inject(source, target):
    with open(source, 'r') as s, open(target, 'w') as t:
        # Strips the newline character
        line = s.readline()
        while line:
            print(line)
            if line.startswith("<?xml"):
                # Remove xml version
                pass
            else:
                write = inyectar_eventos_raton(t, line)
                if write:
                    write = inyectar_dados(s, t, line)
                if write:
                    write = inyectar_estrella(t, line)
                if write:
                    write = injectar_fichas(t, line)
                if write:
                    write = inyectar_boton_tirar_dados(t, line)
                if write:
                    t.write(line)
            line = s.readline()


if __name__ == "__main__":
    import os
    files = [f for f in os.listdir('.') if os.path.isfile(f)]
    for f in files:
        if ('inject' not in f) and ('full' not in f):
            inject(f, f'..\\..\\web\\src\\app\\parques\{f}')
