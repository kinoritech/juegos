import { Component, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ParquesComponent } from "./parques.component";

@Component({
  templateUrl: './8p.svg',
  styleUrls: ['./parques-8-p.component.scss']
})
export class ParquesOchoPuestosComponent extends ParquesComponent {

}

@NgModule({
  declarations: [
    ParquesOchoPuestosComponent,
  ],
  imports: [
    CommonModule,
  ],
})
export class StupidUselessNeededModule {
  // issue tracked by https://github.com/angular/angular/issues/33507
}
