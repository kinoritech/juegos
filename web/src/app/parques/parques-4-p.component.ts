import { Component, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ParquesComponent } from "./parques.component";

@Component({
  templateUrl: './4p.svg',
  styleUrls: ['./parques-4-p.component.scss']
})
export class ParquesCuatroPuestosComponent extends ParquesComponent {

}

@NgModule({
  declarations: [
    ParquesCuatroPuestosComponent,
  ],
  imports: [
    CommonModule,
  ],
})
export class StupidUselessNeededModule {
  // issue tracked by https://github.com/angular/angular/issues/33507
}
