import { Component, OnInit } from '@angular/core';

@Component({
  template: `
    <li class="list-group-item text-color-emphasis bg-background-hl">El color de sus fichas esta indicado por el color de la estrella</li>
    <li class="list-group-item text-color-emphasis bg-background-hl">En su turno el botón de tirar dados se habilita.</li>
    <li class="list-group-item text-color-emphasis bg-background-hl">Durante su turno, puede mover sus fichas y las de los otros jugadores (por ejemplo para comer).</li>
    <li class="list-group-item text-color-emphasis bg-background-hl">Para terminar su turno haga click/tap en la estrella.</li>
  `,
})
export class AyudaParquesComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
