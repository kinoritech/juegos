import { Component, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ParquesComponent } from "./parques.component";

@Component({
  templateUrl: './6p.svg',
  styleUrls: ['./parques-6-p.component.scss']
})
export class ParquesSeisPuestosComponent extends ParquesComponent {

}

@NgModule({
  declarations: [
    ParquesSeisPuestosComponent,
  ],
  imports: [
    CommonModule,
  ],
})
export class StupidUselessNeededModule {
  // issue tracked by https://github.com/angular/angular/issues/33507
}
