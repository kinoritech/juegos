import {
  Component, ElementRef, Inject, NgModule, ViewChild, ViewChildren,
  AfterViewInit, QueryList, OnInit, NgZone, OnDestroy
} from '@angular/core';
import {CommonModule, DOCUMENT} from '@angular/common';
import {Observable, Subject, Subscription, timer} from 'rxjs';
import {BoardBase, BoardConfig, OBSERVABLE_BOARD_CONFIG} from 'boardgame.io-angular';

import {Movida} from './parques.data';
import {ManejoTablero} from "./parques-board.service";
import {startWith, switchMap} from "rxjs/operators";


@Component({
  template: "",
})
export class ParquesComponent extends BoardBase implements AfterViewInit, OnInit, OnDestroy {

  @ViewChild('tablero') private tablerosvg: ElementRef;
  @ViewChild('estrella') private estella: ElementRef;
  @ViewChild('dado1') private dado1: ElementRef;
  @ViewChild('dado2') private dado2: ElementRef;
  @ViewChild('tirarDados') private rollPad: ElementRef;
  @ViewChildren('ficha') private fichasTablero: QueryList<ElementRef>;

  private boardConfig: Observable<BoardConfig>;
  private outsideDrag = null;
  private readonly maxcx = 1045;
  private readonly maxcy = 0;
  private readonly mincx = 0;
  private readonly mincy = -740;

  /** La ficha que esta moviendo el jugador */
  private fichaMovida = null;
  /** Registro de la trayectoria de la ficha movida */
  private trayectoria = [];

  private manejoTablero: ManejoTablero;
  private reset$ = new Subject();
  private subscription: Subscription;
  private wasPing = false;

  constructor(
    @Inject(OBSERVABLE_BOARD_CONFIG) observableBoardConfig: Observable<BoardConfig>,
    @Inject(DOCUMENT) document,
    private _ngZone: NgZone) {
    super(observableBoardConfig);
    this.boardConfig = observableBoardConfig;
  }

  ngOnInit(): void {
    this.subscription = this.reset$.pipe(
        startWith(0),
        switchMap(() => timer(0, 5000))
      )
      .subscribe((i) => {
        console.log('timer ping: ', i, this.wasPing);
        if (this.wasPing) {
          this.wasPing = false;
        } else {
          this.wasPing = true;
          if (this.ctx.currentPlayer !== this.playerID) {
            this.manejoTablero.ping();
          }
        }
    });
  }

  ngAfterViewInit(): void {
    this.manejoTablero = new ManejoTablero(
      this.playerID,
      this.rollPad.nativeElement,
      this.estella.nativeElement,
      this.dado1.nativeElement,
      this.dado2.nativeElement,
      this.fichasTablero.map(ft => ft.nativeElement),
      this.G.alturaFuenteDados,
      this.moves, this.G, this.ctx,);

    this.boardConfig.subscribe((config) => {
      console.log("resetPing", this.wasPing);
      this.manejoTablero.eventoJuego();
      if (!this.wasPing) {
        this.reset$.next(void 0);
      }
    });
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }


  inicioMouse(evt: any): void {
    if (evt.type === 'mousedown' && (evt.target as HTMLElement).classList.contains('ficha')) {
      console.log('inicio mouse', evt);
      evt.stopPropagation();
      evt.preventDefault();
      this.fichaMovida = evt.target;
      console.log('inicio mouse', this.fichaMovida);
      this.trayectoria = [];
      this.outsideDrag = this.arrastre.bind(this);
      window.document.addEventListener('mousemove', this.outsideDrag, {passive: false});
    }
  }

  inicioTouch(evt: any): void {
    if (evt.type === 'touchstart' && (evt.target as HTMLElement).classList.contains('ficha')) {
      // console.log('inicio touch', evt);
      evt.stopPropagation();
      evt.preventDefault();
      this.fichaMovida = evt.target;
      this.trayectoria = [];
      this.outsideDrag = this.arrastre.bind(this);
      window.document.addEventListener('touchmove', this.outsideDrag, {passive: false});
    }
  }

  arrastre(evt: any) {
    if (this.fichaMovida !== null) {
      console.log('arrastre', evt);
      evt.stopPropagation();
      evt.preventDefault();
      this.moverFicha(evt);
    }
  }

  finMouse(evt: any) {
    if (this.fichaMovida !== null) {
      // console.log('finMouse', evt);
      evt.stopPropagation();
      evt.preventDefault();
      this.moverFicha(evt);
      this.moves.moverFicha(this.playerID, this.fichaMovida.id, this.trayectoria);
      this.fichaMovida = null;
      window.document.removeEventListener('mousemove', this.outsideDrag);
    }
  }

  finTouch(evt: any) {
    if (this.fichaMovida !== null) {
      // console.log('finTouch', evt);
      evt.stopPropagation();
      evt.preventDefault();
      this.moverFicha(evt);
      this.moves.moverFicha(this.playerID, this.fichaMovida.id, this.trayectoria);
      this.fichaMovida = null;
      window.document.removeEventListener('touchmove', this.outsideDrag);
    }
  }

  enTirarDados(evt: MouseEvent) {
    // console.log('enTirarDados');
    evt.preventDefault();
    evt.stopPropagation();
    this.manejoTablero.enTirarDados()
    this.moves.lanzarDados();
  }

  enFinTurno(evt: any) {
    // console.log('finTurno', evt);
    evt.preventDefault();
    evt.stopPropagation();
    if (this.manejoTablero.enFinTurno()) {
      console.log('enviar finTurno');
      this.events.endTurn();
    }
  }

  private moverFicha(evt: MouseEvent) {
    const coord = this.coordenadasFicha(evt);
    if (coord !== null) {
      this.actualizarFicha(coord);
      this.trayectoria.push(new Movida(coord.x, coord.y, evt.type, evt.timeStamp));
    }
  }

  private coordenadasFicha(evt) {
    let evtX = null;
    let evtY = null;
    if (evt.type === 'mousemove' || evt.type === 'mouseup' || evt.type === 'mouseleave') {
      evtX = evt.clientX;
      evtY = evt.clientY;
    } else if (evt.type === 'touchmove' || evt.type === 'touchend' || evt.type === 'touchcancel') {
      if (evt.touches.length === 1) {
        evtX = evt.touches[0].clientX;
        evtY = evt.touches[0].clientY;
      } else if (evt.changedTouches.length === 1) {
        evtX = evt.changedTouches[0].clientX;
        evtY = evt.changedTouches[0].clientY;
      }
    }
    let coord = null;
    if (evtX !== null && evtY !== null) {
      coord = this.screenCoordToSvgCoord(evtX, evtY);
    }
    return coord;
  }

  private actualizarFicha(coord: SVGPoint): any {
    console.log('actualizarFicha', this.fichaMovida);
    if (coord !== null) {
      const x = this.fichaMovida.cx.baseVal.value;
      const y = this.fichaMovida.cy.baseVal.value;
      this.fichaMovida.setAttribute('cx', coord.x);
      this.fichaMovida.setAttribute('cy', coord.y);
      return {x, y};
    }
    return null;
  }

  private screenCoordToSvgCoord(evtX, evtY): SVGPoint {
    const punto: SVGPoint = this.tablerosvg.nativeElement.createSVGPoint();
    const ctm = this.fichaMovida.getScreenCTM();
    punto.x = evtX;
    punto.y = evtY;
    const p = punto.matrixTransform(ctm.inverse());
    return p;
  }


}

@NgModule({
  declarations: [
    ParquesComponent,
  ],
  imports: [
    CommonModule,
  ],
})
export class ParquesComponentModule {
  // issue tracked by https://github.com/angular/angular/issues/33507
}
