import {Ficha, Movida} from "./parques.data";
import {environment} from "../../environments/environment";

const EstadosJugador = {
  INICIO_JUEGO: 1,
  TURNO_OTRO_JUGADOR: 2,
  INICIO_TURNO: 3,      // Cuando inicia el turno del jugador, se usa para hacer 1 refresco de las fichas
  EN_TURNO: 4,
};


export class ManejoTablero {

  private estado = EstadosJugador.INICIO_JUEGO;
  private movidasAnterior = -1;
  private tirarDadosPrevio = 0;
  private tiroDados = false;
  private tableroListo = false;
  private jugadorAnterior = -1;
  private readonly esSafari;

  constructor(
    private idJugador,
    private elementoLanzarDados,
    private elementoEstrella,
    private elementoDadoUno,
    private elementoDadoDos,
    private elementosFichas: any[],
    private alturaFuenteDados,
    private movidas,
    private juego,
    private contexto
  ) {
    this.esSafari = /Safari/.test(navigator.userAgent) && /Apple Computer/.test(navigator.vendor);
  }

  /**
   * Resopnde a un nuevo estado del juego.
   */
  eventoJuego() {
    this.manejarTurnos();
    // this.restaurarFichas();
    console.log('eventoJuego post', this.estado);
    switch (this.estado) {
      case EstadosJugador.INICIO_JUEGO:
        console.log('inicio juego');
        this.habilitarLanzarDados(false);
        this.habilitarEstrella(false);
        this.enviarFichas();
        break;
      case EstadosJugador.TURNO_OTRO_JUGADOR:
        console.log('otro jugando');
        this.habilitarLanzarDados(false);
        this.habilitarEstrella(false);
        this.actualizarDados();
        if (this.movidasAnterior !== this.contexto.numMoves) {
          this.movidasAnterior = this.contexto.numMoves;
          if (this.juego.ultimaMovida.length > 0) {
            this.reproducirTrayectoria();
          }
        } else {
          this.restaurarFichas();
        }
        break;
      case EstadosJugador.INICIO_TURNO:
        console.log('turno iniciado');
        this.iniciarTurnoJugador();
        break;
      case EstadosJugador.EN_TURNO:
        console.log('en turno');
        this.actualizarDados();
        break;
    }
  }

  /**
   * Esperar a que todos los jugadores entren al juego, indicado por la fase del juego
   * <p>
   * Si el juego no ha iniciado, reviso si mis fichas están y si no, las envío.
   */
  private manejarTurnos() {
    console.log('manejarTurnos', this.idJugador, this.contexto.phase);
    if (this.contexto.phase === 'inicio') {
      this.estado = EstadosJugador.INICIO_JUEGO;
    } else if (this.contexto.phase === 'jugando') {
      if (this.contexto.currentPlayer === this.idJugador) {
        console.log('miTurno', this.jugadorAnterior);
        if (this.contexto.currentPlayer !== this.jugadorAnterior) {
          this.jugadorAnterior = this.contexto.currentPlayer;
          this.estado = EstadosJugador.INICIO_TURNO;
          this.contexto.events.setActivePlayers({ others: 'moves' });
        }
      } else {
        console.log('otroTurno');
        this.estado = EstadosJugador.TURNO_OTRO_JUGADOR;
        this.jugadorAnterior = this.contexto.currentPlayer;
      }
    }
  }

  private habilitarEstrella(estado: boolean) {
    console.log('habilitarEstrella', estado);
    if (estado) {
      this.elementoEstrella.classList.add('siguiente');
      this.elementoEstrella.disabled = false;
    } else {
      this.elementoEstrella.classList.remove('siguiente');
      this.elementoEstrella.disabled = true;
    }
  }

  enFinTurno() {
    console.log('enFinTurno');
    return this.tiroDados;
  }

  private iniciarTurnoJugador() {
    console.log('iniciarTurnoJugador');
    this.recogerDados();
    this.habilitarLanzarDados(true);
    this.habilitarEstrella(false);
    this.habilitarFichas(true, this.juego);
    this.tiroDados = false;
    this.estado = EstadosJugador.EN_TURNO;
  }

  /**
   * Actualiza la posición d todas las fichas en el tablero, a excepción de la ultima ficha movida (si presente).
   * @param ultimaMovida
   */
  private restaurarFichas() {
    console.log('restaurarFichas', this.tableroListo);
    // if (!this.tableroListo) {
      try {
        const fichasJuego: Ficha[] = Object.values<Ficha>(this.juego.fichas);
        for (const f of fichasJuego) {
          console.log('moverFichas. Ficha juego', f);
          let mover = this.juego.fichaMovida === null ? true : this.juego.fichaMovida !== f.id;
          if (mover) {
            console.log('moverFichas. moviendo', f);
            this.moverFicha(f.id, f)
          }
        }
      } catch (error) {
        // console.log('Error en actualizarPosicionFichas', error);
      }
      // if (this.juego.ultimaMovida.length > 0) {
      //   console.log('hay ultima movida');
      //   this.reproducirTrayectoria(true);
      // }
      // this.tableroListo = true;

    // }
  }

  /**
   * Usa la información de la trayectoria enviada por el servidor para reproducir el desplazamiento de la ficha.
   * Emplea un timer que usa los marcadores de tiempo para la reproducción.
   * @param fichaId
   * @param trayectoria
   */
  private reproducirTrayectoria(inicio = false) {
    const fichaId = this.juego.fichaMovida;
    const trayectoria = this.juego.ultimaMovida;
    console.log('reproducirTrayectoria', fichaId);
    if (inicio || (fichaId.charAt(1) !== this.idJugador)) {
      console.log('reproducirTrayectoria, otro jugador');
      trayectoria.sort((a, b) => {
        if (a.marcaTiempo > b.marcaTiempo) {
          return 1;
        }
        if (b.marcaTiempo > a.marcaTiempo) {
          return -1;
        }
        return 0;
      });
      let tiempo = trayectoria[0].marcaTiempo;
      const reproducir = () => {
        const evento = trayectoria.shift();
        this.moverFicha(fichaId, evento);
        if (trayectoria.length === 0) {
          return;
        }
        const timeOut = evento.marcaTiempo - tiempo;
        tiempo = evento.marcaTiempo;
        setTimeout(reproducir, timeOut);
      };
      reproducir();
    }
  }

  // Clase manejo fichas

  private enviarFichas() {
    const fichasJuego: Ficha[] = Object.values<Ficha>(this.juego.fichas);
    const unaMia = fichasJuego.find(fb => fb.idJugador === this.idJugador);
    if (!unaMia) {
      try {
        const fichasJugador = this.elementosFichas
          .filter(f => f.id.charAt(1) === this.idJugador)
          .map(f => {
            return new Ficha(
              f.cx.baseVal.value,
              f.cy.baseVal.value,
              f.id,
              this.idJugador);
          });
        // console.log('Enviando mis fichas.', fichasJugador);
        this.movidas.ponerFichas(fichasJugador);
      } catch (err) {
        console.error(err);
      }
    }
  }

  //estado = this.ctx.phase === 'jugando' && this.ctx.currentPlayer === this.playerID
  private habilitarFichas(estado, juego) {
    // console.log("habilitarFichas");
    try {
      const fichasJuego: Ficha[] = Object.values<Ficha>(juego.fichas);
      for (const f of fichasJuego) {
        const mf = this.elementosFichas.find(ef => ef.id === f.id);
        if (estado) {
          mf.classList.add('ficha');
        } else {
          mf.classList.remove('ficha');
        }
      }
    } catch (error) {
      console.log('Error en actualizarPosicionFichas', error);
    }
  }

  /**
   * Mueve una ficha a una nueva posición. La ficha se busca por el id dado conta los elementosFichas-
   * @param fichaId   id de la ficha
   * @param posicion  nueva posicion
   */
  private moverFicha(fichaId: string, posicion: { cx, cy }) {
    console.log('moverFicha', fichaId, posicion);
    try {
      const mf = this.elementosFichas.find(fb => fb.id === fichaId);
      mf.setAttribute('cx', posicion.cx);
      mf.setAttribute('cy', posicion.cy);
    } catch (error) {
      console.log('Error en moverUltimaFicha', error);
    }
  }


  // Clase manejo dados

  /**
   * Responde a tirar dados. Una vez se tiran los dados se habilita la estrella.
   */
  enTirarDados() {
    console.log('enTirarDados');
    this.habilitarLanzarDados(false);
    this.habilitarEstrella(true);
    if (this.esSafari) {
      const audio = new Audio(environment.audioDados);
      audio.play();
    }
    this.estado = EstadosJugador.EN_TURNO;
    this.tiroDados = true;
  }

  private actualizarDados() {
    // console.log('actualizar dados', this.tirarDadosPrevio, this.G.tirarDados)
    if (this.tirarDadosPrevio ^ this.juego.tirarDados) {
      this.tirarDadosPrevio = this.juego.tirarDados;
      const valorDados = [...this.juego.valorDados];
      if (!this.esSafari) {
        const audio = new Audio(environment.audioDados);
        audio.play();
      }
      setTimeout(() => {
        let i = 0;
        const rolling = setInterval(() => {
          this.asignarValorDados(valorDados[i], valorDados[i + 1]);
          i += 2;
          if (i > 23) {
            i = 0;
          }
        }, 200);
        setTimeout(() => {
          clearInterval(rolling);
          this.asignarValorDados(valorDados[22], valorDados[23]);
          if (this.estado === EstadosJugador.EN_TURNO) {
            this.habilitarLanzarDados(true);
          }
        }, 2400);
      }, this.esSafari ? 1400 : 0);
    }
  }

  habilitarLanzarDados(estado: boolean) {
    if (estado) {
      this.elementoLanzarDados.classList.remove('noRoll');
      this.elementoLanzarDados.classList.add('roll');
      this.elementoLanzarDados.disabled = false;
    } else {
      this.elementoLanzarDados.classList.remove('roll');
      this.elementoLanzarDados.classList.add('noRoll');
      this.elementoLanzarDados.disabled = true;
    }
  }

  private asignarValorDados(val1: string, val2: string) {
    console.log("asignarValorDados", val1, val2, this.alturaFuenteDados);
    this.elementoDadoUno.textContent = val1;
    this.elementoDadoUno.style.fontSize = this.alturaFuenteDados;
    this.elementoDadoDos.textContent = val2;
    this.elementoDadoDos.style.fontSize = this.alturaFuenteDados;
    this.asignarColorDados();
  }

  private asignarColorDados() {
    this.elementoDadoUno.classList.remove(...this.elementoDadoUno.classList);
    this.elementoDadoUno.classList.add('d' + this.contexto.currentPlayer);
    this.elementoDadoDos.classList.remove(...this.elementoDadoDos.classList);
    this.elementoDadoDos.classList.add('d' + this.contexto.currentPlayer);
  }

  private recogerDados() {
    console.log("recogerDados");
    this.asignarValorDados('-', '-');
  }

  ping() {
    if (this.contexto.phase === 'jugando') {
      console.log("ping");
      this.movidas.ping();
    }
  }

}
