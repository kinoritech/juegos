import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subject } from "rxjs";

import { AlertasService, AutenticacionService, LobbyConnectionService, GameConfig, Room } from '../servicios';
import {first} from "rxjs/operators";

/**
 * Componente que permite crear, ver, unirse, jugar, salir o ver un juego
 */
@Component({
  selector: 'app-inicio',
  templateUrl: './recibidor.component.html',
  styleUrls: ['./recibidor.component.scss'],
})
export class RecibidorComponent implements OnInit {

  infoJuegos: GameConfig[];
  cargando = true;
  actualizarSalas: Subject<boolean> = new Subject<boolean>();
  nombreJugador : string;
  idJuego : string;
  nombreJuego : string;
  bmac_cdnjs = '<script data-name="BMC-Widget"\n' +
    '          src="https://cdnjs.buymeacoffee.com/1.0.0/widget.prod.min.js"\n' +
    '          data-id="KinoriTech"\n' +
    '          data-description="Apoya el desarrollo de este sitio 😃"\n' +
    '          data-message="Gracias por jugar. Si te divertiste, cómpranos un ☕! (o dos 🙃)"\n' +
    '          data-color="#cb4b16"\n' +
    '          data-position="right"\n' +
    '          data-x_margin="55"\n' +
    '          data-y_margin="55"></script>'
  constructor(
    private servicioLoby: LobbyConnectionService,
    private autenticacion: AutenticacionService,
    private alertas: AlertasService,
    private router: Router) {
    this.infoJuegos = servicioLoby.getGameComponents();
  }

  ngOnInit(): void {
    /** Si la cookie no existe, ngx-cookie-service retorna una cadena vacia */
    if (this.autenticacion.fichaUsuario !== "") {
      this.autenticacion.informacionUsuario()
        .pipe(first())
        .subscribe(
          usuario => {
            this.nombreJugador = usuario.id;
            this.cargando = false;
          },
          error => {
            this.alertas.error(error);
            this.router.navigate(['/inicio']);
          }
        )
    } else {
      this.router.navigate(['/inicio']);
      this.nombreJugador = null;
    }
  }

  /**
   * Evento de creación de una sala. Envia la información de la sala al servidor de juegos y
   * espera respuesta.
   * @param informacion información de la sala.
   */
  crearSala(informacion: any) {
    this.servicioLoby.createRoom(informacion.juego, informacion.jugadores)
      .subscribe(roomInfo => {
        this.refrescarSalas();
      });
  }

  /**
   * Evento de dejar sala. Envia la solicitud de dejar sala al servidor de juegos.
   * @param sala sala que se deja
   */
  dejarSala(event: {sala: Room, silla: any}): void {
    for (const player of event.sala.players) {
      if (player.name === this.nombreJugador) {
        this.servicioLoby.leave(event.sala.gameName, event.sala.gameID, event.silla.id, this.autenticacion.reto)
          .subscribe(
            (val) => {
              this.refrescarSalas();
            }
          );
        break;
      }
    }
  }

  /**
   * Evento de unirse a una sala. Enváa la solicitud de unirse a la sala al servidor de juegos.
   * @param event información de la sala y silla para ingresar al juego
   */
  unirseSala(event: {sala: Room, silla: any}): void {
    this.servicioLoby.join(event.sala.gameName, event.sala.gameID, event.silla.id, this.nombreJugador,
        this.autenticacion.fichaUsuario)
      .subscribe(val => {
          this.refrescarSalas();
          this.alertas.success("Ingreso a sala exitoso.");
      },
      (error) => this.alertas.error('volver a intentar'));
  }

  /**
   * Entrar a un juego. Envia la solicitud de empezar el juego al servidor y navega a la pagina del juego
   * @param event
   */
  jugar(event: {sala: Room, silla: any}) {
    this.servicioLoby.configRoom(event.sala);
    this.guardarInformacionJuego(event.sala.gameID, event.sala.gameName)
    this.entrarJuego(event.silla.id);
  }

  entrarJuego(idJugador) {
    this.irAlJuego(idJugador);
  }

  /**
   * Entrar a un juego como espectador.
   * @param sala  la sala donde se desea entrar.
   */
  espectador(sala: Room): void {
    // console.log('espectador', sala);
    this.irAlJuego("", sala.gameID, sala.gameName);
  }

  private irAlJuego(idJugador: string, idSala: string = this.idJuego, nombreJuego: string = this.nombreJuego) {
    console.log("ir al juego", idJugador);
    this.router.navigate(['/juego/' + idJugador, {idSala: idSala, nombreJuego: nombreJuego}]);
  }

  private refrescarSalas() {
    this.actualizarSalas.next(true);
  }

  private guardarInformacionJuego(idJuego: string, nombreJuego: string, guardarCookie = true) {
    this.idJuego = idJuego;
    this.nombreJuego = nombreJuego;
  }

  cerrarSesion() {
    this.autenticacion.terminarSesion();
    this.nombreJugador = null;
    this.router.navigate(['/inicio']);
  }

}
