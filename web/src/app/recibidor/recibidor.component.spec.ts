import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RecibidorComponent } from './recibidor.component';

describe('InicioComponent', () => {
  let component: RecibidorComponent;
  let fixture: ComponentFixture<RecibidorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RecibidorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecibidorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
