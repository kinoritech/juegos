import {Component, EventEmitter, Input, OnInit, Output, TemplateRef } from '@angular/core';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss'],
})
export class NavbarComponent implements OnInit {

  @Input() juego: boolean;
  @Input() itemsAyuda;
  @Output() salir = new EventEmitter<boolean>();


  isCollapsed: boolean = false;
  showRules: boolean = false;

  show = true;

  constructor() { }

  ngOnInit(): void {

  }

  cerrarSesion() {
    this.salir.emit(true);
  }

  dejarJuego() {
    this.salir.emit(false);
  }

  colapse() {
    this.isCollapsed = !this.isCollapsed;
  }

  rulesDropdown() {
    this.showRules = !this.showRules;
  }

}
