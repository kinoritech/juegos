import {Component, ViewChild, OnInit, TemplateRef} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import { BioClientComponent } from 'boardgame.io-angular';

import {AutenticacionService, GameConfig, LobbyConnectionService} from "../servicios";

/**
 * Componente que contiene el tablero de los diferentes juegos.
 */
@Component({
  templateUrl: './juego.component.html',
  styleUrls: ['./juego.component.scss'],
})
export class JuegoComponent implements OnInit {

  @ViewChild('bio') clienteBio: BioClientComponent;

  idJugador: string;
  idJuego: string;
  config: GameConfig;
  credenciales: string;

  constructor(
    private servicioLoby: LobbyConnectionService,
    private autenticacion: AutenticacionService,
    private router: Router,
    private ruta: ActivatedRoute) {
      this.credenciales = this.autenticacion.reto;
  }

  ngOnInit(): void {
    this.ruta.paramMap.subscribe( paramMap => {
      this.idJugador = paramMap.get('id');
      this.idJuego = paramMap.get('idSala');
      this.config = this.servicioLoby.gameConfig(paramMap.get('nombreJuego'));
    });
  }

  cerrarSesion(cerrarSesion) {
    if (cerrarSesion) {
      this.autenticacion.terminarSesion();
      this.router.navigate(['/inicio']);
    } else {
      this.router.navigate(['']);
    }
  }
}
