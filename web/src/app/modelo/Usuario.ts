export class Usuario {
  id: string;
  nombre: string;
  apellido: string;
  clave: string;
  reto: string;
}
