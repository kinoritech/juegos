﻿﻿import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { faEye, faEyeSlash } from '@fortawesome/free-solid-svg-icons';

import { AlertasService, RegistroService, AutenticacionService } from '../servicios';

@Component({
  selector: 'app-forma-registrar-usuario',
  templateUrl: './forma-registrar-usuario.component.html',
  styleUrls: ['./forma-registrar-usuario.component.scss']
})
export class FormaRegistrarUsuarioComponent implements OnInit {

  @ViewChild('clave') private clave: ElementRef;
  formaRegistro: FormGroup;
  pwEye = faEye;
  hide = true;

  cargando = false;
  enviada = false;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private autenticacion: AutenticacionService,
    private usuarios: RegistroService,
    private alertas: AlertasService
  ) {
    // Redirigir al inicio si ya ha iniciadio sesión
    if (this.autenticacion.fichaUsuario !== "") {
      this.router.navigate(['/']);
    }
  }

  ngOnInit() {
    this.formaRegistro = this.formBuilder.group({
      nombre: ['', Validators.required],
      apellido: ['', Validators.required],
      id: ['', Validators.required],
      clave: ['', [Validators.required, Validators.minLength(6)]]
    });
  }

  // método de utilidad para simplificar validación forma
  get f() { return this.formaRegistro.controls; }

  onSubmit() {
    this.enviada = true;
    // reiniciar alarmas al enviar
    this.alertas.clear();
    // no hacer uso del servicio si la forma es invalida
    if (this.formaRegistro.invalid) {
      return;
    }
    this.cargando = true;
    this.usuarios.register(this.formaRegistro.value)
      .subscribe(
        data => {
          this.alertas.success(`${data}. Inicie sesión con su usuario.`, true);
          this.router.navigate(['/inicio']);
        },
        error => {
          this.alertas.error(error);
          this.cargando = false;
        });
  }

  showPass(): void {
    this.hide = !this.hide;
    this.pwEye = this.hide ? faEye : faEyeSlash;
  }
}
