import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormaRegistrarUsuarioComponent } from './forma-registrar-usuario.component';

describe('FormaRegistrarUsuarioComponent', () => {
  let component: FormaRegistrarUsuarioComponent;
  let fixture: ComponentFixture<FormaRegistrarUsuarioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormaRegistrarUsuarioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormaRegistrarUsuarioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
