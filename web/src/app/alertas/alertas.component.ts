import {ChangeDetectorRef, Component, OnDestroy, OnInit} from '@angular/core';
import {style, animate, transition, trigger} from '@angular/animations';
import { Subscription } from "rxjs";

import { AlertasService } from "../servicios";

@Component({
  selector: 'app-alertas',
  templateUrl: './alertas.component.html',
  styleUrls: ['./alertas.component.scss'],
  animations: [
    trigger('fadeInOut', [
      transition(':enter', [   // :enter is alias to 'void => *'
        style({opacity:0, top: -20}),
        animate(500, style({opacity:1, top: 0}))
      ]),
      transition(':leave', [   // :leave is alias to '* => void'
        animate(500, style({opacity:0}))
      ])
    ])
  ]
})
export class AlertasComponent implements OnInit, OnDestroy {

  private subscription: Subscription;
  message: any;

  constructor(private cdRef: ChangeDetectorRef,
              private alertService: AlertasService) { }

  ngOnInit() {
    this.subscription = this.alertService.getAlert()
      .subscribe(message => {
        switch (message && message.type) {
          case 'success':
            message.cssClass = 'alert alert-dismissible border border-success text-success bg-background-hl';
            break;
          case 'error':
            message.cssClass = 'alert alert-dismissible border border-danger text-danger bg-background-hl';
            break;
        }
        this.message = message;
      });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  dismiss() {
    this.message = null;
  }
}
