import { Injectable } from '@angular/core';
import { Observable, Subject, Subscription } from "rxjs";
import { NavigationEnd, Router } from "@angular/router";

@Injectable({
  providedIn: 'root'
})
export class AlertasService {

  private subject = new Subject<any>();
  private keepAfterRouteChange = false;
  private routerEvents: Subscription;

  constructor(private router: Router) {
  }

  getAlert(): Observable<any> {
    return this.subject.asObservable();
  }

  success(message: string, keepAfterRouteChange = false) {
    this.waitForRouteChange(keepAfterRouteChange);
    this.subject.next({type: 'success', text: message});
  }

  error(message: string, keepAfterRouteChange = false) {
    this.waitForRouteChange(keepAfterRouteChange);
    this.subject.next({type: 'error', text: message});
  }

  clear() {
    this.subject.next();
  }

  waitForRouteChange(keepAfterRouteChange) {
    this.keepAfterRouteChange = keepAfterRouteChange;
    this.routerEvents = this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        if (this.keepAfterRouteChange) {
          // only keep for a single route change
          this.keepAfterRouteChange = false;
        } else {
          // clear alert message
          this.clear();
          this.routerEvents.unsubscribe();
        }
      }
    });
  }
}
