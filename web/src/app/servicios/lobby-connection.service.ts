import {Injectable, Inject, Optional} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {map} from 'rxjs/operators';
import {SocketIO, Local} from 'boardgame.io/multiplayer';
import {MCTSBot} from 'boardgame.io/ai';
import _ from "lodash";

export interface Game {
  name: string;
  minPlayers: number;
  maxPlayers: number;
}

export interface GameConfig {
  game: any;
  numPlayers?: number;
  board: any;
  multiplayer?: any;
  ai?: any;
  enhancer?: any;
  debug?: any;
  ayuda: any;
}

export interface Player {
  id: string;
  name: string;
}

export interface Room {
  gameName: string;
  gameID: string;
  players: Player[];
  setupData: any;
}

@Injectable({
  providedIn: 'root'
})
export class LobbyConnectionService {

  //private rooms: Room[] = [];
  private httpOptions = {
    headers: new HttpHeaders({'Content-Type': 'application/json'})
  };

  constructor(
    private http: HttpClient,
    @Inject('gameServer') private gameServer: string,
    @Inject('lobbyServer') @Optional() private readonly lobbyServer: string,
    @Inject('gameComponents') private gameComponents: GameConfig[]) {
    if (lobbyServer === null) {
      this.lobbyServer = gameServer;
    }
    console.log(gameComponents);
  }

  getGameComponents(): GameConfig[] {
    return this.gameComponents;
  }

  createRoom(gameName: string, numPlayers: number): Observable<any> {
    const comp = this.findComponentForGame(gameName);
    if (comp === null) {
      return throwError(new Error('Game not found in available GameComponents'));
    }
    if (
      numPlayers < comp.game.minPlayers ||
      numPlayers > comp.game.maxPlayers
    ) {
      return throwError(new Error('Invalid number of players ' + numPlayers));
    }
    return this.http.post<string>(
      this.baseUrl() + '/' + gameName + '/create',
      {numPlayers},
      this.httpOptions);
  }

  //getRooms(): Observable<Room[]> {
  getGames(): Observable<any[]> {
    console.log("getRooms");
    return this.http.get<any[]>(this.baseUrl(), {observe: 'response'})
      .pipe(
        map(res => {
          console.log("Respuesta", res.status, res.body);
          const games = [];
          _.forOwn(res.body, (v, k) => {
            if (this.findComponentForGame(v)) {
              games.push(v);
            }
          });
          console.log("Juegos encontrados", games);
          return games;
        })
      );
  }

  configRoom(room: Room): void {
    const info: GameConfig = this.gameConfig(room.gameName);
    let multiplayer;
    if (info.numPlayers > 1) {
      multiplayer = SocketIO({server: this.gameServer});
    } else {
      const maxPlayers = info.game.maxPlayers;
      const bots = {};
      for (let i = 1; i < maxPlayers; i++) {
        bots[i + ''] = MCTSBot;
      }
      multiplayer = Local({bots});
    }
    info.multiplayer = multiplayer;
  }

  gameConfig(gameName: string): GameConfig {
    // console.log('gameConfig', gameName);
    for (const comp of this.gameComponents) {
      // console.log('gameComponents', comp.game.name);
      if (comp.game.name === gameName) {
        return comp;
      }
    }
  }

  leave(gameName: string, gameID: string, playerID: string, credentials: string): Observable<any> {
    return this.http.post(this.baseUrl() + '/' + gameName + '/' + gameID + '/leave',
      {playerID, credentials},
      this.httpOptions);
  }

  join(gameName: string, gameID: string, playerID: string, playerName: string, ficha: string): Observable<any> {
    const httpOptns = this.httpOptions;
    httpOptns.headers = httpOptns.headers.set('authorization', `Bearer ${ficha}`);
    return this.http.post(this.baseUrl() + '/' + gameName + '/' + gameID + '/join',
      {playerID, playerName},
      httpOptns);
  }

  getGameDetails(gameName: string): Observable<Room[]> {
    console.log('getGameDetails', gameName);
    return this.http.get<{ rooms: any[] }>(this.baseUrl() + '/' + gameName).pipe(
      map(j => {
        for (const inst of j.rooms) {
          inst.gameName = gameName;
        }
        return j.rooms;
      })
    );
  }

  private findComponentForGame(gameName: string) {
    console.log('findComponentForGame', gameName);
    for (const comp of this.gameComponents) {
      if (comp.game.name === gameName) {
        console.log('encontrado', gameName);
        return comp;
      }
    }
    return null;
  }

  private baseUrl() {
    return this.lobbyServer + '/games';
  }

}
