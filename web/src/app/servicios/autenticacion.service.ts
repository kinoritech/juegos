import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from "rxjs";
import { catchError, map, retry } from "rxjs/operators";
import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";

import { Usuario } from '../modelo/Usuario';
import { environment } from '../../environments/environment';
import { CookieService } from "ngx-cookie-service";
import { ErroresService } from "./errores.service";

@Injectable({
  providedIn: 'root'
})
export class AutenticacionService {

  private fichaSubject: BehaviorSubject<string>;
  private retoSubject: BehaviorSubject<string>;
  public fichaActual: Observable<string>;

  usuario: Usuario;

  constructor(
      private http: HttpClient,
      private galletas: CookieService,
      private errores: ErroresService) {
    let info = this.galletas.get(environment.COOKIE_JUGADOR);
    this.fichaSubject = new BehaviorSubject<string>(info);
    this.retoSubject = new BehaviorSubject<string>(null);
    this.fichaActual = this.fichaSubject.asObservable();
  }

  public get fichaUsuario(): string {
    return this.fichaSubject.value;
  }

  public get reto(): string {
    return this.retoSubject.value;
  }

  informacionUsuario() {
    const headers_object = new HttpHeaders()
        .append('Content-Type', 'application/json')
        .append("Authorization", "Bearer " + this.fichaSubject.value);
    const httpOptions = {
      headers: headers_object
    };
    // FIXME Add a nice error handler to transform http errors into human readable.
    return this.http.post<any>(`${environment.authServer}/auth/infousuario`, {}, httpOptions)
      .pipe(
        retry(1),
        catchError(this.errores.handleError),
        map(usuario => {
          this.usuario = usuario;
          this.retoSubject.next(usuario.reto);
          return usuario;
          })
      );
  }

  /**
   * Solicitar inicio de sesión al servidor de usuarios, utilizando el usuario y clave provistos.
   * @param usuario
   * @param clave
   */
  iniciarSesion(usuario, clave) {
    const body = new HttpParams()
      .set('username', usuario)
      .set('password', clave)
      .set('grant_type', 'password')
      .set('client_id', 'JuegosKinoriTech')
      .set('client_secret', '8D3AA16A77AC5149D49C9E6FBBA2B7702D1364C49E923082B61A02C63F796B2A');
    const headers_object = new HttpHeaders()
          .append('Content-Type', 'application/x-www-form-urlencoded');
    const httpOptions = {
      headers: headers_object
    };
    return this.http.post<any>(`${environment.authServer}/auth/ficha`, body.toString(), httpOptions)
      .pipe(
        catchError(this.errores.handleError),
        map(ficha => {
        console.log('respuesta login', ficha)
        this.galletas.set(environment.COOKIE_JUGADOR, ficha.access_token, environment.VIDA_GALLETAS, environment.cookiePath);
        this.fichaSubject.next(ficha.access_token);
        return ficha.access_token;
      }));
  }

  terminarSesion() {
    // remove user from local storage and set current user to null
    this.galletas.delete(environment.COOKIE_JUGADOR, environment.cookiePath);
    this.fichaSubject.next(null);
    this.retoSubject.next(null);
  }
}
