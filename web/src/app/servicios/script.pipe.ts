import { Pipe, PipeTransform } from '@angular/core';
import {DomSanitizer} from "@angular/platform-browser";

/**
 * Creado por Dhrumil Bhankhar
 * Tomado de https://stackoverflow.com/a/44164070/3837873
 * Nos permite inyectar un script remoto en un componente. En el componente indicado se debe agregar un elemento
 * a la plantilla (por ejemplo un div) con la siguiente estructura:
 * ```
 *  <div [innerHTML]="scriptText | script">
 *  </div>
 * ```
 * donde `scriptText` es una variable del componente que contiene el elemento `<script>...</script>` completo que queremos
 * inyectar.
 */
@Pipe({
  name: 'script'
})
export class ScriptPipe implements PipeTransform {

  constructor(private domSanitizer: DomSanitizer) { }

  handleExternalScriptsInHtmlString(string) {
    const that = this;
    const parser = new DOMParser();
    const scripts = parser.parseFromString(string, 'text/html').getElementsByTagName('script');
    const results = [];

    for (let i = 0; i < scripts.length; i++) {
      const src = scripts[i].getAttribute('src');
      if (src.length && results.indexOf(src) === -1) {
        results.push(src);
        that.addScript(src);
      }
    }
    return results;
  }

  addScript(src) {
    const script = document.createElement('script');
    script.setAttribute('src', src);
    document.body.appendChild(script);
  }


  transform(htmlContent: any) {
    let sanitizeHtmlContent = this.domSanitizer.bypassSecurityTrustHtml(htmlContent);
    this.handleExternalScriptsInHtmlString(htmlContent);
    return sanitizeHtmlContent;
  }

}
