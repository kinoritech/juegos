import { TestBed } from '@angular/core/testing';

import { LobbyConnectionService } from './lobby-connection.service';

describe('LobbyConnectionService', () => {
  let service: LobbyConnectionService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(LobbyConnectionService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
