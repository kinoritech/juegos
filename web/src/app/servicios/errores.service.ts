import { Injectable } from '@angular/core';
import { throwError  } from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class ErroresService {

  constructor() { }

  handleError(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // client-side error
      errorMessage = `Error: ${error.error.message}`;
    } else {
      // server-side error
      switch(error.status) {
        case 201:
          errorMessage = `Success`;
          break;
        case 400:
          errorMessage = `Error de autenticación. Usuario/credenciales invalidas.`;
          break;
        case 404:
          errorMessage = `Error de autenticación. Usuario/credenciales invalidas.`;
          break;
        case 409:
          errorMessage = error.error;
          break;
        case 500:
          errorMessage = `Error del servicio. Contacte a soporte.`;
          break;
        case 0:
          errorMessage = `Servidor no disponible. Contacte a soporte.`;
          break;
        default:
          console.log('Agregar soporte para ', error.status);
          errorMessage = `Error contactando al servicio de autenticación/usuarios. Contacte a soporte.`;
      }
    }
    return throwError(errorMessage);
  }
}
