import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";

import { environment } from '../../environments/environment';
import { Usuario } from "../modelo/Usuario";
import { catchError } from "rxjs/operators";
import { ErroresService} from "./errores.service";

@Injectable({
  providedIn: 'root'
})
export class RegistroService {

  constructor(private http: HttpClient, private errores: ErroresService) { }

  /**
   * Registrar un nuevo usuario.
   * @param usuario
   */
  register(usuario: Usuario) {
    return this.http.post(`${environment.authServer}/auth/registrar`, usuario,{responseType: 'text'})
      .pipe(
        catchError(this.errores.handleError),
      );
  }

}
