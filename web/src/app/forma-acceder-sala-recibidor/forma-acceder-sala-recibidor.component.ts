import {Component, OnInit, Input, OnDestroy, Output, EventEmitter, AfterViewInit} from '@angular/core';
import { Room, LobbyConnectionService } from '../servicios';
import {Subject, of, Subscription, timer, forkJoin} from 'rxjs';
import {catchError, concatMap, map, switchMap, tap} from 'rxjs/operators';

@Component({
  selector: 'app-forma-acceder-sala-recibidor',
  templateUrl: './forma-acceder-sala-recibidor.component.html',
  styleUrls: ['./forma-acceder-sala-recibidor.component.scss']
})
export class FormaAccederSalaRecibidorComponent implements OnInit, AfterViewInit, OnDestroy {

  @Input() nombreJugador: string;
  @Input() actualizarSalas: Subject<boolean> = new Subject<boolean>();
  @Output() enDejarSala = new EventEmitter<{sala: Room, silla: any}>();
  @Output() enJugar = new EventEmitter<{sala: Room, silla: any}>();
  @Output() enUnirseSala = new EventEmitter<{sala: Room, silla: any}>();
  @Output() enEspectador = new EventEmitter<Room>();

  salas: Room[];
  erroresConexion$ = new Subject<boolean>();

  private subscripcion: Subscription;
  cargando: boolean = true;

  constructor(private servicioLoby: LobbyConnectionService) { }

  /**
   * Crea un timer de 3s para refrescar la información de salas disponibles.
   */
  ngOnInit(): void {
    console.log("ngOnInit");
    this.subscripcion = this.actualizarSalas.pipe(
      switchMap(_ => timer(0, 3000).pipe(
        tap(r => console.log("timer pipe", r)),
        catchError((error) => {
          console.error('Error cargando la lista de salas', error);
          this.cargando = false;
          this.erroresConexion$.next(true);
          return of<Room[]>();
        }),
        concatMap(_ => this.servicioLoby.getGames()),
        map(juegos => {
          this.cargando = true;
          const nuevasSalas = [];
          juegos.forEach(g => nuevasSalas.push(this.servicioLoby.getGameDetails(g)
                  .pipe(
                    // tap(info => console.log("Info juego ", info)),
                    catchError(err => of(err))
                  )))
          return nuevasSalas;
        })
        ))
      )
      .subscribe(
        r => {
          // console.log('suscribe', r);
          this.cargando = false;
          forkJoin(r).subscribe(salas => {
            // console.log('mergemap subscribe', salas)
            this.salas = [];
            salas.forEach(s => {
              console.log(s);
              this.salas = this.salas.concat(<Room>s);
            });
          });
        },
          err => {
          console.error('Error cargando la lista de salas', err);
          this.cargando = false;
          this.erroresConexion$.next(true);
      });
  }

  ngAfterViewInit(): void {
    console.log("ngAfterViewInit");
    this.actualizarSalas.next(true);
  }

  /**
   * Cancelar la subscripción a la información de salas.
   */
  ngOnDestroy(): void {
    this.subscripcion.unsubscribe();
  }

  /**
   * Retorna el estado actual de la sala/juego. Si el jugador no esta en la sala el estado es 'ABIERTO', de lo
   * contrado el es 'INICIADO'.
   * @param sala la sala para la que se calcula el estado
   */
  estadoSala(sala: Room): string {
    let estado = 'ABIERTO';
    if (!sala.players.find(player => !this.nombreJugador)) {
      estado = 'INICIADO';
    }
    return estado;
  }

  /**
   * Retrona la lista de jugadores en la sala. En los puestos libres se pone '[libre]'
   * @param sala la sala para la que se crea la lista de jugadores
   */
  jugadoresSala(sala: Room): string {
    return sala.players.map((j) => j.name || '[libre]').join(', ');
  }

  /**
   * Calcula el estado actual de las sillas en la sala. Retorna la silla ocupada por este jugador y
   * la siguiente silla disponible. Un valor de null/undefined en los resultados indica que esa silla
   * no se encuentra disponible.
   * @param sala la sala para la que se calculan el estado de las sillas
   */
  sillas(sala: Room): any[] {
    const jugador = sala.players.find(
        player => player.name === this.nombreJugador
      );
    const libre = sala.players.find(player => !player.hasOwnProperty('name'));
    return [jugador, libre];
  }

  /**
   * Salir de una sala
   * @param sala la sala/juego de la que se sale
   * @param silla
   */
  dejarSala(sala: Room, silla: any): void {
    this.enDejarSala.emit({sala, silla});
  }

  /**
   * Unirse a una sala
   * @param sala la sala/juego al que se une
   * @param silla la silla que se va a ocupar
   */
  unirseSala(sala: Room, silla: any): void {
    this.enUnirseSala.emit({sala, silla});
  }

  /**
   * Ir al tablero de la sala a jugar
   * @param sala la sala/juego al que se une
   * @param silla
   */
  jugar(sala: Room, silla: any): void {
    this.enJugar.emit({sala, silla});
  }

  /**
   * Unirse a un juego como expectador
   * @param sala la sala/juego al que se une
   */
  espectador(sala: Room): void {
    this.enEspectador.emit(sala);
  }

}
