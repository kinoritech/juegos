import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormaAccederSalaRecibidorComponent } from './forma-acceder-sala-recibidor.component';

describe('FormaAccederSalaRecibidorComponent', () => {
  let component: FormaAccederSalaRecibidorComponent;
  let fixture: ComponentFixture<FormaAccederSalaRecibidorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormaAccederSalaRecibidorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormaAccederSalaRecibidorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
