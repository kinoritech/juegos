import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { BoardgameIoModule } from 'boardgame.io-angular';
import { CookieService } from "ngx-cookie-service";
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { JuegoComponent } from './juego/juego.component';
import { RecibidorComponent } from './recibidor/recibidor.component';
import { FooterComponent } from './footer/footer.component';
import { FormaIngresarComponent } from './forma-ingresar/forma-ingresar.component';
import { FormaCrearSalaRecibidorComponent } from './forma-crear-sala-recibidor/forma-crear-sala-recibidor.component';
import { FormaAccederSalaRecibidorComponent } from './forma-acceder-sala-recibidor/forma-acceder-sala-recibidor.component';
import { ParquesOchoPuestosComponent } from './parques/parques-8-p.component';
import { ParquesCuatroPuestosComponent } from "./parques/parques-4-p.component";
import { ParquesSeisPuestosComponent } from "./parques/parques-6-p.component";
import { FormaRegistrarUsuarioComponent } from './forma-registrar-usuario/forma-registrar-usuario.component';
import { AlertasComponent } from './alertas/alertas.component';
import { NavbarComponent } from './navbar/navbar.component';

import { environment } from '../environments/environment';
import { Parques4P, Parques6P, Parques8P } from './parques/parques.data';
import { ScriptPipe } from './servicios/script.pipe';
import { AyudaParquesComponent } from './parques/ayuda-parques.component';


@NgModule({
  declarations: [
    AppComponent,
    RecibidorComponent,
    JuegoComponent,
    FooterComponent,
    FormaIngresarComponent,
    FormaCrearSalaRecibidorComponent,
    FormaAccederSalaRecibidorComponent,
    FormaRegistrarUsuarioComponent,
    AlertasComponent,
    NavbarComponent,
    ScriptPipe,
    AyudaParquesComponent
  ],
  imports: [
    BrowserAnimationsModule,
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    BoardgameIoModule,
    FontAwesomeModule,
  ],
  providers: [
      CookieService,
      { provide: 'gameServer', useValue: environment.gameServer },
      { provide: 'gameComponents', useValue: [
        {
          board: ParquesOchoPuestosComponent,
          numPlayers: 8,
          game: Parques8P,
          ayuda: AyudaParquesComponent
        },
        {
          board: ParquesSeisPuestosComponent,
          numPlayers: 6,
          game: Parques6P,
          ayuda: AyudaParquesComponent
        },
        {
          board: ParquesCuatroPuestosComponent,
          numPlayers: 4,
          game: Parques4P,
          ayuda: AyudaParquesComponent
        },
      ] },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
