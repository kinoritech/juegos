import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { JuegoComponent } from './juego/juego.component';
import { RecibidorComponent } from './recibidor/recibidor.component';
import { FormaRegistrarUsuarioComponent } from "./forma-registrar-usuario/forma-registrar-usuario.component";
import { FormaIngresarComponent } from "./forma-ingresar/forma-ingresar.component";


const routes: Routes = [
  { path: '', component: RecibidorComponent },
  { path: 'inicio', component: FormaIngresarComponent },
  { path: 'juego', component: JuegoComponent },
  { path: 'juego/:id', component: JuegoComponent },
  { path: 'registrar', component: FormaRegistrarUsuarioComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
