import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormaIngresarComponent } from './forma-ingresar.component';

describe('FormaRecibidorComponent', () => {
  let component: FormaIngresarComponent;
  let fixture: ComponentFixture<FormaIngresarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormaIngresarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormaIngresarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
