import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { first } from "rxjs/operators";
import { AlertasService, AutenticacionService } from "../servicios";
import { faEye, faEyeSlash } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-forma-recibidor',
  templateUrl: './forma-ingresar.component.html',
  styleUrls: ['./forma-ingresar.component.scss']
})
export class FormaIngresarComponent implements OnInit {

  @ViewChild('clave') private clave: ElementRef;
  formaInicio: FormGroup;
  pwEye = faEye;
  hide = true;

  cargando = false;
  enviada = false;

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private autenticacion: AutenticacionService,
    private alertas: AlertasService
  ) {  }

  ngOnInit() {
    this.formaInicio = this.formBuilder.group({
      usuario: ['', Validators.required],
      clave: ['', Validators.required]
    });
  }

  // convenience getter for easy access to form fields
  get f() { return this.formaInicio.controls; }

  onSubmit() {
    this.enviada = true;

    // reset alerts on submit
    this.alertas.clear();

    // stop here if form is invalid
    if (this.formaInicio.invalid) {
      return;
    }
    console.log('submit')
    this.cargando = true;
    this.autenticacion.iniciarSesion(this.f.usuario.value, this.f.clave.value)
      .pipe(first())
      .subscribe(
        data => {
          console.log('sesion iniciada', data, this.f.usuario.value);
          this.router.navigate(['']);
        },
        error => {
          console.log('error on iniciar sesion')
          this.alertas.error(error);
          this.cargando = false;
        });
  }

  showPass(): void {
    this.hide = !this.hide;
    this.pwEye = this.hide ? faEye : faEyeSlash;
  }

}
