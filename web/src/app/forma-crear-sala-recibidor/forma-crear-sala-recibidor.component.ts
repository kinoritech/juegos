import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-forma-crear-sala-recibidor',
  templateUrl: './forma-crear-sala-recibidor.component.html',
  styleUrls: ['./forma-crear-sala-recibidor.component.scss']
})
export class FormaCrearSalaRecibidorComponent {

  constructor() { }

  @Input() juegos: any[];
  @Output() enCrear = new EventEmitter<{juego: string, jugadores: number}>();

  juegoSeleccionado: number;
  numeroJugadores: number;
  rangoJugadores: number[];

  /**
   * Cmabiar a otro juego
   * @param indiceJuego número del juego al que se cambió
   */
  cambiarJuego(indiceJuego) {
    this.juegoSeleccionado = indiceJuego;
    this.numeroJugadores = this.juegos[indiceJuego].game.minPlayers;
    this.rangoJugadores = [...new Array(this.juegos[indiceJuego].game.maxPlayers + 1).keys()]
      .slice(this.juegos[indiceJuego].game.minPlayers);
  }

  /**
   * Crear una sala con base en la información de juego y número de jugadores seleccionados
   */
  crearSala() {
    this.enCrear.emit({
      juego: this.juegos[this.juegoSeleccionado].game.name,
      jugadores: this.numeroJugadores});
  }

}
