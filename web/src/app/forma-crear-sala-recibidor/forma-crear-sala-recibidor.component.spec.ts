import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormaCrearSalaRecibidorComponent } from './forma-crear-sala-recibidor.component';

describe('FormaCrearSalaRecibidorComponent', () => {
  let component: FormaCrearSalaRecibidorComponent;
  let fixture: ComponentFixture<FormaCrearSalaRecibidorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormaCrearSalaRecibidorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormaCrearSalaRecibidorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
