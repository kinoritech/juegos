export const environment = {
  production: true,
  gameServer: 'http://hoyossanchez.ddns.net:8000',
  authServer: 'http://hoyossanchez.ddns.net:8002',
  audioDados: '/juegos/assets/RollDice.mp3',
  cookiePath: '/juegos',
  VIDA_GALLETAS: 10,
  COOKIE_JUGADOR: 'tech.kinori_jgdr',
};
