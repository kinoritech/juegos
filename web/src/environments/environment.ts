// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  gameServer: 'http://172.16.229.113:8100',
  authServer: 'http://172.16.229.113:8102',
  audioDados: '/assets/RollDice.mp3',
  cookiePath: '',
  VIDA_GALLETAS: 10,
  COOKIE_JUGADOR: 'tech.kinori_jgdr',
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
