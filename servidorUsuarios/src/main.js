"use strict";
const pino = require('pino');

const Autenticacion = require('./oauth');
const config = require('./environments/config');

const dest = pino.destination(config.logDestination);
const log = pino({name: 'usuarios', level: config.loglevel}, dest);

const aut = new Autenticacion();
aut.iniciarDB()
    .then(db => {
        aut.iniciarAccessoDB(db);
        aut.agregarOAuth();
        aut.montarRutas()
        return aut.app.listen(config.port);
    })
    .then(() => {
        log.info(`Autenticación usuarios sirviendo en ${config.port}`);
    })
    .catch(err =>
        log.error("Error iniciando el servidor: %s", err));
