/*
 * Copyright 2020. Kinori Tech
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
 * to permit persons to whom the Software is  furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions
 * of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
"use strict";

/**
 * @module ManejoDatosOAuth
 *
 * El grupo de servicios relativas a OAuth para el manejo de usuarios y credenciales.
 */

/**
 * @constructor
 *
 * @param usuarios      provee CRUD de usuarios en la db
 * @param secretos      provee CRUD de secretos en la db
 */
function Servicios(usuarios) {
    this.usuarios = usuarios
}

/**
 * Registra un usuario en el sistema. El cuerpo de la solicitud debe contener la información del usuario: usuario,
 * clave, nombre, apellido. El método verifica que la información recibida sea valida (no debe haber campos vacíos) y
 * que un usuario con el mismo 'nombre de usuario' no exista. Si la validación pasa y el usuario no existe,
 * se procede a registrar el usuario en la base de datos y se retorna el éxito/fracaso de esa operación.
 */
Servicios.prototype.registrarUsuario = function() {
    const that = this;
    return async function (ctx, next) {
        ctx.log.debug('Registrar usuario');
        const id = ctx.request.body.id;
        const clave = ctx.request.body.clave;
        // Datos válidos
        if (!isString(id) || !isString(clave)) {
            ctx.log.error('Credenciales invalidas');
            ctx.throw(400, 'Credenciales invalidas');
        } else {
            //query db to see if the user exists already
            const exists = await that.usuarios.existe(id);
            if (exists) {
                ctx.log.warn('Usuario ya existe');
                ctx.throw(409, 'Usuario ya existe. Seleccione otro.');
            } else {
                const nombre = ctx.request.body.nombre;
                const apellido = ctx.request.body.apellido;
                // Datos válidos
                if (!isString(nombre) || !isString(apellido)) {
                    ctx.log.error('Información de usuario no valida.');
                    ctx.throw(400, 'Información de usuario no valida. Nombre y/o apellido no pueden estar vacíos.');
                } else {
                    const nuevoUsuario = await that.usuarios.crear({
                        id,
                        clave,
                        nombre,
                        apellido});
                    if (nuevoUsuario) {
                        ctx.log.info('Usuario registrado exitosamente. %o', nuevoUsuario);
                        ctx.status = 201;
                        ctx.body = `Usuario ${nuevoUsuario.usuario} registrado exitosamente.`;
                    } else {
                        ctx.log.error('Error registrando usuario.');
                        ctx.throw(500, 'Información de usuario no almacenada. Vuelva a intentarlo.');
                    }
                }
            }
        }
    }
}

/**
 * Retorna las credenciales del usuario
 */
Servicios.prototype.credenciales = function() {
    const that = this;
    return async function (ctx, next) {
        ctx.log.info('credenciales');
        try {
            if (ctx.state.oauth.token.accessToken) {
                try {
                    ctx.body = that.usuarios.credenciales(ctx.params.id);
                    ctx.log.info('credenciales encontradas. %o', {id: ctx.params.id, credenciales: ctx.body});
                    return next();
                } catch (err) {
                    ctx.log.error('Error buscando credenciales usuario: %s', err.message)
                    ctx.throw(401, err.message);
                }
            }
        } catch (err) {
            ctx.log.error('Error buscando credenciales usuario - petición sin autenticación.')
            ctx.status = 401 ;
            ctx.body = 'Acceso no ha sido autenticado.';
            ctx.app.emit('error', err, ctx);
        }
    }
}

/**
 * Retorna información sobre al usuario autenticado.
 */
Servicios.prototype.infoUsuario = function() {
    const that = this;
    return async function (ctx, next) {
        ctx.log.debug('infoUsuario');
        try {
            if (ctx.state.oauth.token && ctx.state.oauth.token.accessToken) {
                ctx.log.info('infoUsuario usuario. %o', {id: ctx.state.oauth.token.user});
                try {
                    ctx.body = that.usuarios.informacion(ctx.state.oauth.token.user.id);
                    ctx.log.info('información encontrada. %o', ctx.body);
                    return next();
                } catch (err) {
                    ctx.log.error('Error buscando información usuario: %s', err.message)
                    ctx.throw(401, err.message);
                }
            }
        } catch (err) {
            ctx.log.error('Error buscando información usuario - petición sin autenticación.')
            ctx.status = 401 ;
            ctx.body = 'Acceso no ha sido autenticado.';
            ctx.app.emit('error', err, ctx);
        }
    }
}

/**
 * Verifica que las credenciales sean del usuario y sean correctas.
 */
Servicios.prototype.validarCredenciales = function() {
    const that = this;
    return async function (ctx, next) {
        ctx.log.info('validarCredenciales.  %o', {id: ctx.params.id, credenciales: ctx.request.body.credentials});
        let ficha;
        try {
            if (that.usuarios.validarCredenciales(ctx.params.id, ctx.request.body.credentials)) {
                ctx.log.info('Credenciales válidas');
                ctx.status = 202;
                ctx.body = "Success";
            } else {
                ctx.log.warn('Credenciales invalidas');
                ctx.status = 406;
                ctx.body = "Invalid credentials";
            }
            return next();

        } catch (err) {
            ctx.log.error('Error validando credenciales ', err.message);
            ctx.app.emit('error', err, ctx);
        }
    }
}

/**
 *
 * Returns true the specified parameters is a string else it returns false
 *
 * @param parameter - the variable we're checking is a String
 * @return {boolean}
 */
function isString(parameter) {
    return parameter != null && (typeof parameter === "string"
        || parameter instanceof String)
}

/**
 * Exportar constructor.
 */

module.exports = Servicios;
