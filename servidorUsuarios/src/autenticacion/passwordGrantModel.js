const pino = require('pino');
const _ = require('lodash');

const config = require('../environments/config');

const dest = pino.destination(config.logDestination);
const log = pino({name: 'usuarios', level: config.loglevel}, dest);

/**
 * @module PasswordGrantModel
 *
 * El servidor OAuth2Server require in modelo con una API especifica para el almacenamiento, búsqueda y validación.
 * Este módule ofrece el API requerida para manejar un Password Grant (https://oauth2-server.readthedocs.io/en/latest/model/overview.html#password-grant).
 * Por este motivo, los nombres de los métodos y sus parámetros se mantienen en ingles.
 */


/**
 * @param usuarios  objeto de acceso a usuarios
 * @param fichas    objecto de acceso a fichas
 * @constructor
 */
function PasswordGrantModel(usuarios, fichas) {
    this.usuarios = usuarios
    this.fichas = fichas
}

/**
 * OAuth: Invoked to retrieve an existing access token previously saved through Model#saveToken().
 *
 * Este método es invocado cuando una solicitud ('request') se hace con un portador de ficha (bearer token); el acceso
 * a otras rutas una vez el usuario se ha autenticado se hace de este modo. El método simplemente valida que la ficha
 * que viene con la solicitud sea valido. La validación se hace delegando a la base de datos la búsqueda del usuario
 * al que se le ha asignado la ficha. Si el usuario no existe, la ficha no es válida.
 *
 * El método retorna un objeto una ficha de acceso e información sobre el usuario.
 *
 * @param fichaPortador
 * @returns {{accessTokenExpiresAt: Date, client: {id: (string|{type}|string)}, accessToken: *, user: *}}
 */
PasswordGrantModel.prototype.getAccessToken = function(fichaPortador) {
    log.info('getAccessToken. %o', {fichaPortador});
    const token = this.fichas.buscar(fichaPortador);
    if (!token) {
        log.info('No se ha encontrado usuario para la ficha.')
        return { };
    }
    const userInfo = this.usuarios.buscar(token.user)
    if (!userInfo) {
        log.info('No se ha encontrado usuario para la ficha.')
        return { };
    }
    const data = {
        accessToken: token.accessToken,
        accessTokenExpiresAt: new Date(token.accessTokenExpiresAt),
        client: { id: token.clientId},
        user: _.omit(userInfo, 'clave')
    }
    return data;
}

/**
 * OAuth: Invoked to retrieve a client using a client id or a client id/client secret combination, depending on the
 * grant type.
 *
 * Este método retorna la información del cliente que esta solicitando una ficha de acceso. El cliente se debe buscar
 * utilizando el ID y el secreto del cliente. En aplicaciones web el concepto de cliente es mas difícil de definir.
 * Por tal motivo, en esta implementación en particular, no hacemos verificación de las credenciales del cliente.
 */
PasswordGrantModel.prototype.getClient = function(idCliente, secreto) {
    log.info("getClient. %o", { idCliente, secreto });
    return  {
        clientID: idCliente,
        clientSecret: secreto,
        grants: ["password"]
    }
}

/**
 * OAuth: Invoked to retrieve a user using a usuario/clave combination.
 *
 * Se busca un cliente que responda al nombre de usuario y clave dadas.
 */
PasswordGrantModel.prototype.getUser = function (usuario, clave) {
    log.info('getUser. %o ', { usuario, clave});
    const user = this.usuarios.validar(usuario, clave);
    log.info('usuario encontrado', user);
    if (user === undefined) {
        return false;
    }
    return {
        id: usuario
    };
}

/**
 * OAuht: Invoked to save an access ficha and optionally a refresh token, depending on the grant type.
 *
 * Guarda la ficha de acceso.
 */
PasswordGrantModel.prototype.saveToken = function(ficha, cliente, usuario) {
    log.info('saveToken. %o', {ficha, cliente, usuario})
    const data = {
        accessToken: ficha.accessToken,
        accessTokenExpiresAt: ficha.accessTokenExpiresAt,
        client: cliente,
        refreshToken: ficha.refreshToken,
        refreshTokenExpiresAt: ficha.refreshTokenExpiresAt,
        user: usuario.id
    };
    return this.fichas.guardar(data);
}

/**
 * Export constructor.
 */
module.exports = PasswordGrantModel;
