const pino = require('pino');
const _ = require('lodash');

const config = require('../environments/config');
const dest = pino.destination(config.logDestination);
const log = pino({name: 'usuarios', level: config.loglevel}, dest);

/**
 * @module secretos
 * Implementa métodos CRUD y adicionales para fichas.
 *
 */

/**
 * Crea un nuevo acceso a fichas
 * @param lowDB     base de datos
 * @constructor
 */
function Fichas(lowDB) {
    this.lowDB = lowDB;
}

/**
 * Guarda la ficha de acceso en la base de datos contra el usuario identificado en la ficha.
 *
 * @param fichaAcceso   ficha a guardar
 */
Fichas.prototype.guardar = function(fichaAcceso) {
    log.info('guardar: ', fichaAcceso);
    const previous = this.lowDB.get('fichas')
        .find({user: fichaAcceso.user})
        .value();
    let q;
    if (previous) {
        log.warn('Ficha encontrada, reemplazando')
        q = this.lowDB.get('fichas')
            .find({user: fichaAcceso.user})
            .assign(fichaAcceso)
    } else {
        log.info('Agregando ficha')
        q = this.lowDB.get('fichas')
            .push(fichaAcceso)
    }
    return q.write().then(updated => {
        log.info('Ficha creada', updated);
        if (Array.isArray(updated)) {
            return _.last(updated);
        } else {
            return updated;
        }
    })
}

/**
 * Busca la ficha de acceso asociada con la ficha de portador dada con el fin de retornar el usuario asociado a la
 * ficha de acceso.
 *
 * El método aprovecha para borrar todos los tokens expirados.
 *
 * @param fichaPortador
 */
Fichas.prototype.buscar = function buscar(fichaAcceso) {
    log.info('buscar ficha: %s', fichaAcceso);
    const ficha = this.lowDB.get('fichas')
        .find({ accessToken: fichaAcceso })
        .value();
    // Borrar expirados
    this.lowDB.get('fichas')
        .remove(f => (f.accessToken ===  fichaAcceso) && (new Date(f.accessTokenExpiresAt) <  new Date()))
        .remove()
        .value();
    return ficha;
}

/**
 * Exportar constructor.
 */

module.exports = Fichas;
