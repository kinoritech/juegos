const bcrypt = require('bcrypt');
const crypto = require('crypto');
const pino = require('pino');
const util = require('util');
const _ = require('lodash');

const config = require('../environments/config');


/**
 * @module usuarios
 * Implementa métodos CRUD y adicionales para usuarios.
 *
 */
const dest = pino.destination(config.logDestination);
const saltRounds = 10;
const bcryptGenSalt = util.promisify(bcrypt.genSalt);
const bcryptHash = util.promisify(bcrypt.hash);

/**
 * Crea un nuevo acceso a usuarios.
 * @param lowDB     base de datos
 * @constructor
 */
function Usuarios(lowDB) {
    this.lowDB = lowDB;
    this.log = pino({name: 'usuarios', level: config.loglevel}, dest);
}

/**
 * Crear un usuario en la base de datos con los datos provistos. La clave de usuario es ofuscada. Se crea el
 * verificador para usar como credenciales para Boardgame.io.
 * *
 * @param infoUsuario   los datos del usuario
 */
Usuarios.prototype.crear = function(infoUsuario) {
    this.log.debug('crear');
    const verificador = base64URLEncode(crypto.randomBytes(32));
    return bcryptGenSalt(saltRounds)
        .then(salt => {
            return bcryptHash(infoUsuario.clave, salt);
        })
        .then(hash => {
            return this.lowDB.get('usuarios')
                .push({
                    id: infoUsuario.id,
                    clave: hash,
                    nombre: infoUsuario.nombre,
                    apellido: infoUsuario.apellido,
                    verificador: verificador
                    })
                .write()
                .then(updated => {
                    let final;
                    if (Array.isArray(updated)) {
                        final = _.last(updated);
                    } else {
                        final = updated;
                    }
                    this.log.info('Creado. %o', final);
                    return final;
                });
        });
}

/**
 * Valida que exista un Usuario con el id y clave dadas.
 *
 * @param id        el id de usuario
 * @param clave     la clave
 */
Usuarios.prototype.validar = function(id, clave) {
    this.log.debug('validar. %o', {id, clave});
    return this.lowDB.get('usuarios')
        .find( u => {
            return u.id === id &&  bcrypt.compareSync(clave, u.clave);
        })
        .value();
}

/**
 * Determina si el id de usuario ya existe en la base de datos.
 *
 * @param id   el nombre de usuario
 */
Usuarios.prototype.existe = function(id) {
    this.log.debug('existe. %o', {id});
    const infoUsuario = this.buscar(id);
    return infoUsuario !== undefined;
}

/**
 * Buscar un Usuario usando el id
 *
 * @param id   el id de usuario
 */
Usuarios.prototype.buscar = function(id) {
    this.log.debug('buscar. %o', {id});
    return this.lowDB.get('usuarios')
        .find({id: id})
        .value();
}

/**
 * Retorna las credenciales del usuario, si existe.
 * @param id
 * @returns {*}
 */
Usuarios.prototype.credenciales = function(id) {
    this.log.debug('credenciales. %o', {id});
    const infoUsuario = this.buscar(id);
    if (!infoUsuario) {
        this.log.error('Usuario invalido. %o', { id} );
        throw('Usuario invalido');
    }
    return base64URLEncode(sha256(infoUsuario.verificador));
}

/**
 * Validar las credenciales del usuario
 * @param id
 * @param credenciales
 * @returns {boolean}
 */
Usuarios.prototype.validarCredenciales = function(id, credenciales) {
    this.log.debug('validarCredenciales. %o', {id, credenciales});
    const infoUsuario = this.buscar(id);
    if (!infoUsuario) {
        this.log.error('Usuario invalido. %o', {id, credenciales});
        throw('Usuario invalido');
    }
    const reto = base64URLEncode(sha256(infoUsuario.verificador));
    const resultado = reto === credenciales;
    this.log.debug('validar credenciales. %o', { id, resultado});
    return resultado;
}

/**
 * Retorna la información de usuario, filtrada para no exponer información sensible.
 *
 * @param id
 * @returns {string|null|string}
 */
Usuarios.prototype.informacion = function(id) {
    this.log.debug('informacion. %o', {id});
    const infoUsuario = this.buscar(id);
    if (!infoUsuario) {
        this.log.error('Usuario desconocido. %o', {id});
        throw('Usuario invalido');
    }
    const resultado = { ...infoUsuario };
    resultado.reto = base64URLEncode(sha256(infoUsuario.verificador));
    delete resultado.verificador;
    delete resultado.clave;
    this.log.info('informacion: %o', resultado);
    return resultado;
}

function base64URLEncode(str) {
    return str.toString('base64')
        .replace(/\+/g, '-')
        .replace(/\//g, '_')
        .replace(/=/g, '');
}

function sha256(buffer) {
    return crypto.createHash('sha256').update(buffer).digest();
}

/**
 * Exportar constructor.
 */

module.exports = Usuarios;
