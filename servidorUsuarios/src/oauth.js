/*
 * Copyright 2020. Kinori Tech
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
 * to permit persons to whom the Software is  furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions
 * of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

"use strict";
const Koa = require('koa');
const cors = require('@koa/cors');
const koaBody = require('koa-body');
const mount = require('koa-mount');
const kpino = require('koa-pino-logger')
const Router = require('koa-router');
const KoaOAuthServer = require('koa2-oauth-server');
const low = require('lowdb');
const LowAdapter = require('lowdb/adapters/FileAsync');
const pino = require('pino');

const PasswordGrantModel = require('./autenticacion/passwordGrantModel');
const Servicios = require('./autenticacion/servicios');
const Usuarios = require('./db/usuarios');
const Fichas = require('./db/fichas');
const { ExcepcionEstadoIlegal } = require('./excepciones');
const config = require('./environments/config');

/**
 * @module OAuth
 *
 * El grupo de servicios de OAuth incluye operaciones relativas a las actividades de OAuth the la aplicación,
 * incluyendo funciones tales como autenticación, autorización, descubrimiento, inicio de sesión, manejo de fichas
 * (tokens) y manejo de acceso (grants).
 */


/**
 * @constructor
 */
function OAuth() {
    const dest = pino.destination(config.logDestination);
    this.log = pino({name: 'oatuh', level: config.loglevel}, dest);
    this.app = new Koa();
    this.app.use(kpino({name: 'oatuh', level: config.loglevel}, dest));
    this.app.use(koaBody());
    this.app.use(cors({
        origin: config.corsOrigin,
        allowMethods: ['GET', 'POST']
    }))
    // Parche para OAuth Response que espera que el ctx.response.headers tenga el prototipo de objeto.
    this.app.use(async function(ctx, next) {
        const oldHeaders = { ...ctx.response.headers};
        Object.defineProperty(ctx.response, 'headers', {
            get() {
                return oldHeaders;
            }
        });
        return next();
    });
}

/**
 * Conectar la base de datos al archivo en disco y proveer los valores iniciales.
 */
OAuth.prototype.iniciarDB = async function() {
    this.log.debug('Iniciando DB');
    const adaptador = new LowAdapter(config.usersDb);
    const db = await low(adaptador);
    await db.defaults({usuarios: [], fichas: [], }).write();
    this.log.info('DB iniciada');
    return db;
}

/**
 * Iniciar los módulos de acceso a la base de datos
 * @param lowDb
 */
OAuth.prototype.iniciarAccessoDB = function(lowDb) {
    this.log.debug('Iniciando objetos acceso DB');
    this.usuarios = new Usuarios(lowDb);
    this.fichas = new Fichas(lowDb);
    this.log.info('Objetos acceso DB iniciados');
}

/**
 * Configurar el OAuthServer y agregarlo al servidor koa.
 *
 * Lanza una ExcepcionEstadoIlegal si {@link OAuth.iniciarAccessoDB} no ha sido llamado.
 */
OAuth.prototype.agregarOAuth = function() {
    if (!this.usuarios || !this.fichas) {
        throw new ExcepcionEstadoIlegal('Acceso a base de datos no iniciado. Debe invocar iniciarAccessoDB primero.');
    }
    this.app.oauth = new KoaOAuthServer({
        model: new PasswordGrantModel(this.usuarios, this.fichas),
        useErrorHandler: true,
        accessTokenLifetime: 8 * 60 * 60,       // 8 horas
        grants: ['password'],
        debug: true
    });
}

/**
 * Monta las rutas the OAuth en la ruta base '/auth'.
 *
 * Lanza una ExcepcionEstadoIlegal si {@link OAuth.agregarOAuth} y {@link OAuth.iniciarAccessoDB} no han sido llamados.
 */
OAuth.prototype.montarRutas = function() {
    if (!this.usuarios) {
        throw new ExcepcionEstadoIlegal('Acceso a base de datos no iniciado. Debe invocar iniciarAccessoDB primero.');
    }
    if (!this.app.oauth) {
        throw new ExcepcionEstadoIlegal('OAuth no configurado. Debe invocar `agregarOAuth` primero.');
    }
    const oauthRouter = crearRouter(
            this.app.oauth,
            new Servicios(this.usuarios));
    this.app
        .use(mount('/auth', oauthRouter.routes()))
        .use(oauthRouter.allowedMethods());
}

/**
 *
 * @param router - we assign routes and endpoint functions for each route
 *                  to this object.
 *
 * @param expressApp - an instance of the express app. By applying
 *                     expressApp.oauth.grant() method to an endpoint
 *                     the endpoint will return a bearer token
 *                     to the client if it provides calid credentials.
 *
 * @param servicios - an object which contains the registration method. It
 *                           can be populated with other methods such as deleteUser()
 *                           if you decide to build out of this project's structure.
 * @return {router}
 */
function crearRouter(oauth, servicios) {
    const router = new Router();
    agregarRutaRegistrar(router, servicios);
    agregarRutaFicha(router, oauth);
    agregarRutaInfoUsuario(router, oauth, servicios);
    agregarRutaCredenciales(router, oauth, servicios);
    agregarRutaValidarCredenciales(router, servicios);
    return router;
}

/**
 * Crea un nuevo usuario en el sistema. Utiliza HTTP POST.
 *
 * El servicio Proveedor de OAuth soportar la creación de nuevos usuarios en el sistema.
 *
 * - Método HTTP:   POST
 * - URL:           https://{oauth-server}/{oauth-base}/registrar
 * - Body:
 *      - usuario:  Nombre de usuario. El nombre de usuario es almacenado y retornado como un valor que distingue
 *                  mayúsculas y minúsculas. El nombre de usuario debe ser unico.
 *      - nombre:   Nombre(s) del usuario.
 *      - apellido: Apellido(s) del usuario.
 *      - clave:    La clave del usuario en texto simple. Esta clave será ofuscada y el valor recibido nunca será
 *                  almacenado y no puede ser recuperado.
 * - Content-Type:  application/json
 * - Respuesta:     La respuesta de esta API contiene el Usuario que fue creado. La clave y otros campos sensitivos
 *                  no son retornados.
 *
 * @param {Router} router - El router al que se le agrega la ruta al punto-final
 * @param {Servicios} servicios - objecto que provee la implementación del API
 */
function agregarRutaRegistrar(router, servicios) {
    /* Agregar ruta para registrar usuarios */
    router.post('/registrar', servicios.registrarUsuario());
}

/**
 * Retorna una Ficha de Acceso para el usuario. Utiliza HTTP POST
 *
 * El servicio Proveedor de OAuth soportar la recuperación de Fichas de Acceso a través de su punto-final "Ficha".
 * Esta API es un recurso manejado por OAuth. La implementación actual solo soporta la recuperación de Fichas de Acceso
 * a cambio de concesiones tipo Contraseña
 * ({@link https://oauth2-server.readthedocs.io/en/latest/model/overview.html#password-grant|Password Grant}).
 *
 * - Método HTTP:   POST
 * - URL:           https://{oauth-server}/{oauth-base}/ficha
 * - Parámetros:
 *      - username:         <id/nombre de usuario>
 *      - password:         <clave de usuario>
 *      - grant_type:       password
 *      - client_id:        <id del Cliente>
 *      - client_secret:    <secreto del Cliente>
 * - Content-Type:  application/x-www-form-urlencoded
 * - Respuesta:       Si exitosa, retorna una respuesta con código de estado HTTP 200 y la Ficha de Acceso
 * @param {Router} router
 * @param {KoaOAuthServer} oauth
 */
function agregarRutaFicha(router, oauth) {
    router.post('/ficha', oauth.token());
}

/**
 * Retorna información sobre al usuario autenticado. Utiliza HTTP POST.
 *
 * El servicio Proveedor de OAuth soportar la recuperación de postulados acerca del usuario-final a través de su
 * punto-final "InfoUsuario". Esta API es un recurso protegido por OAuth. Para obtener los postulados solicitados
 * acerca del usuario-final, el Cliente debe hacer una solicitud a la API UserInfo utilizando un a Ficha de Acceso
 * obtenida a través dela API Ficha.
 *
 * - Método HTTP:     POST
 * - URL:             https://{oauth-server}/{oauth-base}/infousuario
 * - Encabezado:      Authorization: Bearer <TOKEN>
 * - Respuesta:       Si exitosa, retorna una respuesta con código de estado HTTP 200 y la información del usuario
 *
 * @see {@link agregarRutaFicha}
 * @param {Router} router - El router al que se le agrega la ruta al punto-final
 * @param {KoaOAuthServer} oauth - El objecto OAuth que usa este servidor
 * @param {Servicios} servicios - objecto que provee la implementación del API
 */
function agregarRutaInfoUsuario(router, oauth, servicios) {
    router.post('/infousuario', oauth.authenticate(), servicios.infoUsuario());
}

function agregarRutaCredenciales(router, oauth, servicios) {
    /* This is the route used by the game server to validate that a user has provided valid
    credentials.
     */
    router.get('/:id/credenciales', oauth.authenticate(), servicios.credenciales());
}

function agregarRutaValidarCredenciales(router, servicios) {
    /* This is the route used by the game server to validate the user credentials
     */
    router.post('/:id/validar-credenciales', servicios.validarCredenciales());
}

module.exports = OAuth;