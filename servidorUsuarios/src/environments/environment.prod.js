module.exports = {
  port: 8002,
  corsOrigin: 'http://hoyossanchez.ddns.net:8001',
  usersDb: 'var/lib/kinoritech/juegos/users/users_db.json',
  loglevel: 'info',
  logDestination: '/var/log/kinoritech/servidorUsuarios.log'
}