module.exports = {
  port: 8102,
  corsOrigin: 'http://172.16.229.113:8101',
  usersDb: 'users_db.json',
  loglevel: 'debug',
  logDestination: 'D:\\temp\\logs\\servidorUsuarios.log'
}